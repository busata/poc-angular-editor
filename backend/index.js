var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');

var app = express();
var articles = {
    1: [{id: 1, content: 'Woo', associations:[]}, {id: 2, content: 'Intro',associations:[]}, {id: 3, content: 'Paragraph',associations:[]}],
    2: [{id: 1, content: 'Kop', associations:[]}, {id: 2, content: 'Intro',associations:[]}, {id: 3, content: 'Paragraph',associations:[]}]
};

function getParagraph(articleId, paragraphId) {
    var paragraphs = articles[articleId];
    return paragraphs.filter((paragraph) => paragraph.id == paragraphId)[0];
}


app.use(cors());
app.use(bodyParser.json());

app.get('/api/articles', (req, res) => {
    res.send(Object.keys(articles));
});

app.post('/api/articles', (req, res) => {
    var id = Math.floor(Math.random()*10000);
    articles[id] = [{id:1, content:"", associations:[]}];
    res.send({
        id
    })
});

app.get('/api/:articleid/paragraphs', (req, res) => {
    res.send(articles[req.params.articleid]);
});

app.put('/api/:articleid/paragraphs/:paragraphid', (req, res) => {
    var paragraph = getParagraph(req.params.articleid,req.params.paragraphid);
    paragraph.content = req.body.content;
    res.send({});
});

app.post('/api/:articleid/paragraphs/:paragraphid', (req, res) => {
    var paragraph = getParagraph(req.params.articleid,req.params.paragraphid);
    var index = articles[req.params.articleid].indexOf(paragraph);
    console.log(req.body);
    articles[req.params.articleid].splice(index + 1, 0, req.body);
    res.send({});
});

app.post('/api/:articleid/paragraphs/:paragraphid/associations', (req, res) => {
    var paragraph = getParagraph(req.params.articleid,req.params.paragraphid);
    paragraph.associations.push(req.body);
    res.send({});
});

app.post('/api/:articleid/paragraphs/:paragraphid/_movedown', (req, res) => {
    var paragraph = getParagraph(req.params.articleid,req.params.paragraphid);
    var fromIndex = articles[req.params.articleid].indexOf(paragraph);
    articles[req.params.articleid].splice(fromIndex,1);
    articles[req.params.articleid].splice(fromIndex+1,0,paragraph);
    res.send({});
});
app.post('/api/:articleid/paragraphs/:paragraphid/_moveup', (req, res) => {
    var paragraph = getParagraph(req.params.articleid,req.params.paragraphid);
    var fromIndex = articles[req.params.articleid].indexOf(paragraph);
    articles[req.params.articleid].splice(fromIndex,1);
    articles[req.params.articleid].splice(fromIndex-1,0,paragraph);
    res.send({});
});

app.listen(3000, () => {
    console.log('Example app start!');
});