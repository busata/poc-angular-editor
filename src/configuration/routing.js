export function routingConfig($stateProvider, $sceProvider, $urlRouterProvider) {
    $sceProvider.enabled(false);
    $urlRouterProvider.otherwise('/');
    $stateProvider.state('editor', {
        url: '/',
        template: '<dashboard></dashboard>'
    }).state('article', {
        url: '/article/:articleid',
        controller:function($scope, $stateParams) {
            $scope.articleId = $stateParams.articleid;
        },
        template: '<article-editor article-id="articleId"></article-editor>'
    })
}