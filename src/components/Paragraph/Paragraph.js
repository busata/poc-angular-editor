require('./Paragraph.scss');
export function Paragraph() {
    return {
        scope: {
            content: '<',
            onUpdate: '&',
            onEnter: '&',
            canMoveUp: '&',
            canMoveDown: '&',
            onFocus: '&',
            hasFocus: '&',
            moveUp: '&',
            moveDown: '&',
            onPaste: '&'
        },
        controllerAs: '$ctrl',
        controller: function() {
            this.doAction = function(actionType) {
                document.execCommand(actionType, false, null);
            }
        },
        bindToController: true,
        template: `<div class="contenteditable-wrapper">
    <div class="toolbar">
        <button class="btn btn-link" ng-click="$ctrl.doAction('bold')"><i class="fa fa-bold"></i></button>
        <button class="btn btn-link" ng-click="$ctrl.doAction('italic')"><i class="fa fa-italic"></i></button>
        <button class="btn btn-link" ng-click="$ctrl.doAction('underline')"><i class="fa fa-underline"></i></button>
        <button class="btn btn-link" ng-if="$ctrl.canMoveUp()" ng-click="$ctrl.moveUp()"><i class="fa fa-arrow-up"></i></button>
        <button class="btn btn-link" ng-if="$ctrl.canMoveDown()" ng-click="$ctrl.moveDown()"><i class="fa fa-arrow-down"></i></button>

    </div>
    <div class="contenteditable" ng-class="{focused: $ctrl.hasFocus()}" contenteditable="true">
    </div>
</div>
        `,
        link: function (scope, element, attrs, ctrl) {
            let editable = angular.element(element[0].querySelector('.contenteditable'));
            editable[0].focus();
            editable[0].innerHTML = ctrl.content;

            editable.on('focus', () => {
                scope.$apply(() => ctrl.onFocus());
            });
            editable.on('input', () => {
                ctrl.onUpdate({content: editable[0].innerHTML});
            });

            editable.on('paste', (evt) => {
                evt.preventDefault();
                scope.$apply(() => {
                    ctrl.onPaste({data:evt.clipboardData.getData('text')});
                })
            });

            editable.on('keydown', (evt) => {
                scope.$apply(() => {
                    if (evt.keyCode == '13') {
                        evt.preventDefault();
                        ctrl.onEnter();
                    }
                });
            })
        }
    }
}