describe('Component: ArticleEditor', () => {
   let $compile, $rootScope, $httpBackend;
   let element;

   beforeEach(angular.mock.module('app'));
   beforeEach(angular.mock.inject(function (_$compile_, _$rootScope_,_$httpBackend_) {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      $httpBackend = _$httpBackend_;
   }));

   afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
   });

   beforeEach(() => {
      $httpBackend.when('GET','http://localtest.me:3000/api/1/paragraphs').respond([{id:1,content:"Hello"},{id:2, content:"World"}]);
      $rootScope.articleId = 1;
      element = $compile('<article-editor article-id="articleId"></article-editor>')($rootScope);
      $httpBackend.flush();
      $rootScope.$digest();
   });

   it('should say hello to the world', ()=> {
      let firstParagraph = element[0].querySelectorAll('.contenteditable')[0];
      let secondParagraph = element[0].querySelectorAll('.contenteditable')[1];
      expect(firstParagraph.innerHTML).toBe('Hello');
      expect(secondParagraph.innerHTML).toBe('World');
   });

   it('should move the paragraph up', ()=> {
      let firstParagraph = element[0].querySelectorAll('.toolbar')[0].querySelectorAll('button');
      $httpBackend.expect('POST','http://localtest.me:3000/api/1/paragraphs/1/_movedown').respond();
      firstParagraph[3].click();
      $httpBackend.flush();
   });


});