require('./ArticleEditor.scss');
export function ArticleEditor(EditorService, $window) {
    return {
        scope: {
            articleId: '<'
        },
        controllerAs: '$ctrl',
        controller: function ($scope) {
            this.lastFocusedParagraphId = -1;
            EditorService.getParagraphs(this.articleId).then((paragraphs) => {
                this.paragraphs = paragraphs;
            });

            $window.addEventListener('message', (message) => {
                EditorService.associate(this.articleId, this.lastFocusedParagraphId, "IMAGE", message.data.data[0]);
                $scope.$digest();
            });

            this.onPaste = function (paragraphId, data) {
                EditorService.associate(this.articleId, paragraphId, "OTHER", data);
            };

            this.updateParagraphContent = function (paragraphId, content) {
                EditorService.updateParagraph(this.articleId,paragraphId, content);
            };

            this.createNewParagraph = function (paragraphId) {
                EditorService.createNewParagraph(this.articleId, paragraphId);
            };

            this.moveDown = function (paragraphId) {
                EditorService.moveDown(this.articleId, paragraphId);
            };

            this.onFocus = function (paragraphId) {
                this.lastFocusedParagraphId = paragraphId;
            };

            this.canMoveDown = function (paragraphId) {
                return EditorService.canMoveDown(this.articleId, paragraphId);
            };
            this.moveUp = function (paragraphId) {
                EditorService.moveUp(this.articleId, paragraphId);
            };

            this.canMoveUp = function (paragraphId) {
                return EditorService.canMoveUp(this.articleId, paragraphId);
            };

            this.createUrl = function (association) {
                return "http://v1.rendercache.test.persgroep.net/rest/diocontent/" + association.src + "/_fit/230/137?appId=0e0c3f2ea7d956f98f982b2508b142ac"
            }
        },
        bindToController: true,
        template: `
        <div class="article-editor">
            <Paragraph ng-repeat-start="paragraph in $ctrl.paragraphs track by paragraph.id"
                       content="paragraph.content"
                       on-focus="$ctrl.onFocus(paragraph.id)"
                       has-focus="paragraph.id == $ctrl.lastFocusedParagraphId"
                       on-update="$ctrl.updateParagraphContent(paragraph.id, content)"
                       can-move-down="$ctrl.canMoveDown(paragraph.id)"
                       move-down="$ctrl.moveDown(paragraph.id)"
                       move-up="$ctrl.moveUp(paragraph.id)"
                       can-move-up="$ctrl.canMoveUp(paragraph.id)"
                       on-paste="$ctrl.onPaste(paragraph.id, data)"
                       on-enter="$ctrl.createNewParagraph(paragraph.id)">
            </Paragraph>
            <div ng-repeat-end>
            <div class="association" ng-repeat="associated in paragraph.associations">
            <img ng-if="associated.type == 'IMAGE'" ng-src="{{$ctrl.createUrl(associated)}}"/>
            <div ng-if="associated.type == 'OTHER'" ng-bind-html="associated.src"></div>
            </div>
            </div>
        </div>
        <div class="image-picker">
            <iframe width="400px" height="600px" src="http://imagepicker.test.persgroep.net/?brand=demorgen"></iframe>
        </div>
        `,
        link: function (scope, element, attrs, ctrl) {

        }
    }
}