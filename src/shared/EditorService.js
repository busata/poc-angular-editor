export function EditorService($http) {
    let paragraphs = [];
    let id = Math.random() * 1000;
    return {
        getParagraphs(articleId) {
            return $http.get('http://localtest.me:3000/api/'+articleId+'/paragraphs').then((response) => {
                paragraphs = response.data;
                return paragraphs;
            });
        },
        createNewParagraph(articleId, paragraphId) {
            let paragraph = paragraphs.filter((paragraph) => paragraph.id == paragraphId)[0];
            var newParagraph = {
                id: id++,
                content: "",
                associations: []
            };
            paragraphs.splice(paragraphs.indexOf(paragraph)+1,0, newParagraph);
            return $http.post('http://localtest.me:3000/api/'+articleId+'/paragraphs/'+paragraphId, newParagraph);
        },
        associate(articleId, paragraphId, type, dioContentId) {
            let paragraph;
            if(paragraphId) {
                paragraph = paragraphs.filter((paragraph => paragraph.id == paragraphId))[0]
            } else {
                paragraph = paragraphs[paragraphs.length - 1];
            }

            console.log(paragraph);

            paragraph.associations.push({
                type: type,
                src: dioContentId
            });

            $http.post('http://localtest.me:3000/api/'+articleId+'/paragraphs/'+paragraphId+'/associations',{
                type: type,
                src: dioContentId
            })
        },
        updateParagraph(articleId, paragraphId, content) {
            let paragraph = paragraphs.filter((paragraph) => paragraph.id == paragraphId)[0];
            paragraphs.content = content;
            $http.put('http://localtest.me:3000/api/'+articleId+'/paragraphs/'+paragraphId, {
                content: content
            })
        },
        canMoveUp(articleId, paragraphId) {
            let paragraph = paragraphs.filter((paragraph) => paragraph.id == paragraphId)[0];
            return paragraphs.indexOf(paragraph) != 0;
        },
        canMoveDown(articleId, paragraphId) {
            let paragraph = paragraphs.filter((paragraph) => paragraph.id == paragraphId)[0];
            return paragraphs.indexOf(paragraph) != (paragraphs.length - 1);
        },
        moveUp(articleId, paragraphId) {
            let paragraph = paragraphs.filter((paragraph) => paragraph.id == paragraphId)[0];
            let fromIndex = paragraphs.indexOf(paragraph);
            paragraphs.splice(fromIndex,1);
            paragraphs.splice(fromIndex-1,0,paragraph);

            return $http.post('http://localtest.me:3000/api/'+articleId+'/paragraphs/'+paragraphId+'/_moveup')
        },
        moveDown(articleId, paragraphId) {
            let paragraph = paragraphs.filter((paragraph) => paragraph.id == paragraphId)[0];
            let fromIndex = paragraphs.indexOf(paragraph);
            paragraphs.splice(fromIndex,1);
            paragraphs.splice(fromIndex+1,0,paragraph);

            return $http.post('http://localtest.me:3000/api/'+articleId+'/paragraphs/'+paragraphId+'/_movedown')
        }
    }
}