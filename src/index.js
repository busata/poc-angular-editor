require('angular-ui-router');
import angular from 'angular';
import {routingConfig} from './configuration/routing';

import {Paragraph} from 'components/Paragraph/Paragraph';
import {ArticleEditor} from 'components/ArticleEditor/ArticleEditor';
import {Dashboard} from 'components/Dashboard/Dashboard';
import {EditorService} from 'shared/EditorService';

angular.module('app', ['ui.router']).config(routingConfig)
    .directive('articleEditor', ArticleEditor)
    .directive('paragraph', Paragraph)
    .directive('dashboard', Dashboard)
    .factory('EditorService', EditorService);
